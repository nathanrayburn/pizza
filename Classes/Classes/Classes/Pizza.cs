﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes.Model
{
    public class Pizza
    {

        #region Private attributes
        private int num;
        private List<Component> listofComponents = null;
        private int pastaType;
        private float price;
        private int newId;


        #endregion Private attributes

        #region constructor
        //Storing our needed data to private variables
        public Pizza(int num, int pastaType, List<Component> listOfComponents)
        {
            this.num = num;
            this.pastaType = pastaType;
            this.listofComponents = listOfComponents;

        }
        //Ovveriding the ToString function to be able to display the orders in the list
        public override string ToString()
        {
            return "Commande num." + this.num;
        }

        #endregion constructor

        #region acccessor




        public List<Component> ListofComponents
        {

            set { this.listofComponents = value; }
            get { return this.listofComponents; }


        }


        public int PastaType
        {
            set { this.pastaType = value; }
            get { return this.pastaType; }

        }

        public int ID
        {

            set { this.num = value; }
            get { return this.num; }
        }
        public float Price
        {

            get
            {


                this.price = 0; //Reset the price so we can recalculate it

                //Add the price of all ingredients that are on the pizza
                foreach (Component ingredient in this.listofComponents)
                {
                    this.price += ingredient.UnitPrice;

                }
                //Add the price of the pastaType
                switch (pastaType)
                {
                    case 0:
                        this.price += 10;
                        break;
                    case 1:
                        this.price += 11;
                        break;
                    case 2:
                        this.price += 20;
                        break;
                    default:
                        break;


                }
                return this.price; //returns the pizza's price

            }
        }

        public int newID
        {

            set { this.newId = value; }
            get
            {
                CSV content = new CSV();
                var read = content.ReadCommands;
                int count = 1;
                bool restart = true;
                do
                {
                    restart = false;
                    foreach (string line in read)
                    {
                        
                        var list = line.Split(';').ToList<string>();
                        int.TryParse(list[0], out count);
                        if (count == this.newId)
                        {
                            this.newId++;
                            restart = true;

                        }
          
                    }
            

                } while (restart);



                return this.newId;

            }

        } 

        }
        #endregion acccessor
    }

