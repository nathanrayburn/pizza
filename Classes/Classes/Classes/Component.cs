﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes.Model
{
    public class Component
    {

        #region Private attributes
        private string name = null;
        private float unitPrice;
        private List<string> ingredients;


        #endregion Private attributes
        #region constructor

        public Component(string name, float unitPrice)
        {
            this.name = name;
            this.unitPrice = unitPrice;

        }
        #endregion constructor

        #region accessor

        public string Name
        {
            set { this.name = value; }
            get { return this.name; }

        }

        public float UnitPrice
        {

            set { }
            get { return this.unitPrice; }

        }




        #endregion accessor
    }
}
