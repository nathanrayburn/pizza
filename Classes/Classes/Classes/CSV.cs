﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes.Model
{
    public class CSV
    {


        #region private
        private string path = @"..\..\..\csv\data.csv";
        private string pathPrice = @"..\..\..\csv\price.csv";
        private string date;
        private string file;
        private List<string> content;
       

        #endregion private

        #region constructor


        #endregion constructor

        #region accessor
        public List<string> ReadCommands
        {


            get
            {
                this.content = new List<string>();
                using (StreamReader rd = new StreamReader(path))
                {

                    string secondLine = File.ReadLines(path).ElementAtOrDefault(0);
                    while ((secondLine = rd.ReadLine()) != null)
                    {
                        this.content.Add(secondLine);

                    }


                    rd.Close(); //close the file

                }
                return this.content;
            }
        }

        public List<string> ReadIngredients
        {


            get
            {
                this.content = new List<string>();
                using (StreamReader rd = new StreamReader(pathPrice))
                {

                    string secondLine = File.ReadLines(pathPrice).ElementAtOrDefault(0);
                    while ((secondLine = rd.ReadLine()) != null)
                    {
                        this.content.Add(secondLine);

                    }


                    rd.Close(); //close the file

                }
                return this.content;
            }
        }
        public void Write(Pizza all)
        {

            CSV content = new CSV();
            List<string> lstCommandes = new List<string>();
            List<string> ingredients = new List<string>();
            //Delcaring our string variables to combine later on
            string numbersAdd = "";
            string ingredientsAdd = "";
            string combine = "";
            var data = content.ReadCommands; //Storing the csv data

            //Setting to data we need to private variables
            int id = all.ID; 
            float price = all.Price;
            int pastaType = all.PastaType;


            //Convert to string
            string idAdd = id.ToString();
            string priceAdd = price.ToString();
            string pastaTypeAdd = pastaType.ToString();

            numbersAdd = idAdd  + ";" + priceAdd + ";" + pastaTypeAdd; //Create a string with the 3 first indexes
            
            var count = data.Count;
            //Removes the order from the CSV file content, so we can add our new updated order
            for (int i = count - 1; i >= 0; i--)
            {
                lstCommandes = data[i].Split(';').ToList();
                var tempID = 0;
                Int32.TryParse(lstCommandes[0], out tempID);
                if (tempID == id)
                {

                    data.RemoveAt(i);

                }


            }
            //Add all ingredients to a list of string
            foreach (Component line in all.ListofComponents)
            {

                ingredients.Add(line.Name); 


            }

            ingredientsAdd = string.Join(";", ingredients); //Creating one string of the list of ingredients with a seperator
            combine = numbersAdd + ";" + ingredientsAdd; //Combine the first 3 indexes with the ingredients
            data.Add(combine); //Add the order to the csv file content

            //Writing into the csv file
            using (StreamWriter wr = new StreamWriter(path)) //Opens the file
            {


                foreach (String s in data)
                    wr.WriteLine(s); //Write line into the csv file


                wr.Close(); //close the file

            }


        }

        #endregion accessor



    }
}
