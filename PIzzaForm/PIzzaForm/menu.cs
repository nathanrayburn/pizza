﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Classes.Model;

namespace PIzzaForm
{
    public partial class formConcept : Form
    {
      
        private Pizza send = null;
        private List<string> listPrices = new List<string>();
        private List<Component> allIngredients = new List<Component>();

        


        public formConcept(Pizza send,List<string> listPrices)
        {
            InitializeComponent();
            this.send = send;
            this.listPrices = listPrices;
        }

        /**
         * This private function returns the default list of all ingredients
         * return List<Component>
         * */
        private List<Component> createIngredients()
        {
            string name = null;
            float price = 0; ;
            foreach (string line in listPrices)
            {
              
          
               var listAvalable = line.Split(';').ToList(); //split de array of the csv content and turns into a list of string
                name = listAvalable[0];
                float.TryParse(listAvalable[1], out price);
                Component ingredient = new Component(name,price);
                allIngredients.Add(ingredient); //Adding the ingredient to a list of components
            }

            return allIngredients;

        }


        private void GroupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void ListView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Grp_Ingredients_Load(object sender, EventArgs e)
        {
            List<string> listAvalable = new List<string>();
            List<string> avalable = new List<string>();
            listPrices.RemoveAt(0);

            foreach (Component components in send.ListofComponents)
            {
                if (components.Name != "")

                {
                    chkList_Ingredients_Chosen.Items.Add(components.Name); //Adding all chosen items from the order to the list

                }
  
                avalable.Clear();
                foreach (string prices in listPrices)
                {
                    
                    listAvalable = prices.Split(';').ToList(); //split de array of the csv content and turns into a list of string
                    listAvalable.RemoveAt(1); //Removing the price
                    avalable.AddRange(listAvalable); //Adding the Ingredient to the default list

                }

            }
                //Filtering the default ingredients list to have only avalable items left
                foreach (Component pizzaComponent in send.ListofComponents)
                {
                    for (int i = avalable.Count - 1; i >= 0; i--)
                    {
                 
                        if (avalable.Contains(pizzaComponent.Name))
                           avalable.Remove(pizzaComponent.Name);
                    }
                }

            foreach (string line in avalable)
            {
                chkList_Ingredients_Avalible.Items.Add(line); //Adding the avalable items to the avalable list
            }

            if (send.ListofComponents.Count == 0)
            {
                var list = createIngredients(); //recover the list of components by default (all ingredients)
                foreach(Component line in list)
                {
                    chkList_Ingredients_Avalible.Items.Add(line.Name);

                }
                list.Clear();

            }

            lbl_NumCommande_Data.Text = send.ID.ToString() ; //Updating the ID of the order when load
            lbl_Price_Data.Text = send.Price.ToString(); //Updating the Price tag of the pizza
            cmb_Pasta.SelectedItem = cmb_Pasta.Items[send.PastaType]; //Selecting the combo box's pastaType what was from the order

        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Btn_Selector_Right_Click(object sender, EventArgs e)
        {
            if (chkList_Ingredients_Chosen.SelectedItem == "") { }
            else
            {



                var selected = chkList_Ingredients_Chosen.SelectedItem; //Storing the name of the selected item
                var count = send.ListofComponents.Count(); //counter
                //Removing the Ingredient from the Pizza and updating the price
                for (int i = count - 1; i >= 0; i--)
                {
                    if (send.ListofComponents.ElementAt(i).Name == selected)
                    {

                        send.ListofComponents.RemoveAt(i); //Removing the ingredient that's in the pizza
                        lbl_Price_Data.Text = send.Price.ToString(); //Recalculating the price

                    }

                }
                //Updating the view
                
                chkList_Ingredients_Avalible.Items.Add(chkList_Ingredients_Chosen.SelectedItem);
                chkList_Ingredients_Chosen.Items.Remove(chkList_Ingredients_Chosen.SelectedItem);

                if (chkList_Ingredients_Chosen.Items.Count == 0)
                {

                    btn_Selector_Right.Enabled = false;
                }
                if (chkList_Ingredients_Avalible.Items.Count > 0)
                {

                    btn_Selector_Left.Enabled = true;
                }

            }
        }

        private void Btn_Selector_Left_Click(object sender, EventArgs e)
        {

            if (chkList_Ingredients_Avalible.SelectedItem == "") { } //Need to manage  the exception if the index is null TODO

            else
            {


                var selected = chkList_Ingredients_Avalible.SelectedItem; //Recovering the selected item name
                var list = createIngredients(); //recover the list of components by default (all ingredients)
                var count = list.Count;
                //Adding the ingredient to the pizza and updating the price 
                for (int i = count - 1; i >= 0; i--)
                {
                   
                    if (list[i].Name == selected.ToString())
                    {

                        send.ListofComponents.Add(list.ElementAt(i)); //Adding the ingredient to the pizza 


                        lbl_Price_Data.Text = send.Price.ToString(); //Recalculating the price

                    }


                }
                //Updating the view

                chkList_Ingredients_Chosen.Items.Add(chkList_Ingredients_Avalible.SelectedItem);
                chkList_Ingredients_Avalible.Items.Remove(chkList_Ingredients_Avalible.SelectedItem);
                if (chkList_Ingredients_Avalible.Items.Count == 0)
                {

                    btn_Selector_Left.Enabled = false;
                }
                if (chkList_Ingredients_Chosen.Items.Count > 0)
                {

                    btn_Selector_Right.Enabled = true;
                }
                list.Clear(); //Clearing the default list
            }
        }

        private void Cmb_Pasta_SelectedIndexChanged(object sender, EventArgs e)
        {
            send.PastaType = cmb_Pasta.SelectedIndex; //Changing the pizza's pastaType
            lbl_Price_Data.Text = send.Price.ToString(); //Recalculating the price
        }

        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            CSV write = new CSV();
            write.Write(send); //Save the current pizza to the file
  
           DialogResult = DialogResult.OK;
        }

        private void ChkList_Ingredients_Chosen_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
