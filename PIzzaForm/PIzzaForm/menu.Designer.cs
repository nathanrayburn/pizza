﻿namespace PIzzaForm
{
    partial class formConcept
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grp_Ingredients_unChosen = new System.Windows.Forms.GroupBox();
            this.chkList_Ingredients_Avalible = new System.Windows.Forms.CheckedListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkList_Ingredients_Chosen = new System.Windows.Forms.CheckedListBox();
            this.btn_Selector_Right = new System.Windows.Forms.Button();
            this.btn_Selector_Left = new System.Windows.Forms.Button();
            this.lbl_NumCommande = new System.Windows.Forms.Label();
            this.lbl_Price = new System.Windows.Forms.Label();
            this.lbl_NumCommande_Data = new System.Windows.Forms.Label();
            this.lbl_Price_Data = new System.Windows.Forms.Label();
            this.cmb_Pasta = new System.Windows.Forms.ComboBox();
            this.grp_Validation = new System.Windows.Forms.GroupBox();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.btn_Save = new System.Windows.Forms.Button();
            this.grp_data = new System.Windows.Forms.GroupBox();
            this.grp_Pasta = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.grp_Ingredients_unChosen.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.grp_Validation.SuspendLayout();
            this.grp_data.SuspendLayout();
            this.grp_Pasta.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.grp_Ingredients_unChosen);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btn_Selector_Right);
            this.groupBox1.Controls.Add(this.btn_Selector_Left);
            this.groupBox1.Location = new System.Drawing.Point(12, 203);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(590, 297);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.GroupBox1_Enter);
            // 
            // grp_Ingredients_unChosen
            // 
            this.grp_Ingredients_unChosen.Controls.Add(this.chkList_Ingredients_Avalible);
            this.grp_Ingredients_unChosen.Location = new System.Drawing.Point(310, 27);
            this.grp_Ingredients_unChosen.Name = "grp_Ingredients_unChosen";
            this.grp_Ingredients_unChosen.Size = new System.Drawing.Size(166, 243);
            this.grp_Ingredients_unChosen.TabIndex = 0;
            this.grp_Ingredients_unChosen.TabStop = false;
            this.grp_Ingredients_unChosen.Text = "Ingrédients Disponibles";
            // 
            // chkList_Ingredients_Avalible
            // 
            this.chkList_Ingredients_Avalible.FormattingEnabled = true;
            this.chkList_Ingredients_Avalible.Location = new System.Drawing.Point(22, 33);
            this.chkList_Ingredients_Avalible.Name = "chkList_Ingredients_Avalible";
            this.chkList_Ingredients_Avalible.Size = new System.Drawing.Size(120, 184);
            this.chkList_Ingredients_Avalible.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkList_Ingredients_Chosen);
            this.groupBox2.Location = new System.Drawing.Point(26, 35);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(160, 235);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ingrédients choisis";
            // 
            // chkList_Ingredients_Chosen
            // 
            this.chkList_Ingredients_Chosen.FormattingEnabled = true;
            this.chkList_Ingredients_Chosen.Location = new System.Drawing.Point(14, 19);
            this.chkList_Ingredients_Chosen.Name = "chkList_Ingredients_Chosen";
            this.chkList_Ingredients_Chosen.Size = new System.Drawing.Size(120, 199);
            this.chkList_Ingredients_Chosen.TabIndex = 2;
            this.chkList_Ingredients_Chosen.SelectedIndexChanged += new System.EventHandler(this.ChkList_Ingredients_Chosen_SelectedIndexChanged);
            // 
            // btn_Selector_Right
            // 
            this.btn_Selector_Right.Location = new System.Drawing.Point(215, 164);
            this.btn_Selector_Right.Name = "btn_Selector_Right";
            this.btn_Selector_Right.Size = new System.Drawing.Size(75, 35);
            this.btn_Selector_Right.TabIndex = 3;
            this.btn_Selector_Right.Text = ">>";
            this.btn_Selector_Right.UseVisualStyleBackColor = true;
            this.btn_Selector_Right.Click += new System.EventHandler(this.Btn_Selector_Right_Click);
            // 
            // btn_Selector_Left
            // 
            this.btn_Selector_Left.Location = new System.Drawing.Point(215, 101);
            this.btn_Selector_Left.Name = "btn_Selector_Left";
            this.btn_Selector_Left.Size = new System.Drawing.Size(75, 31);
            this.btn_Selector_Left.TabIndex = 1;
            this.btn_Selector_Left.Text = "<<";
            this.btn_Selector_Left.UseVisualStyleBackColor = true;
            this.btn_Selector_Left.Click += new System.EventHandler(this.Btn_Selector_Left_Click);
            // 
            // lbl_NumCommande
            // 
            this.lbl_NumCommande.AutoSize = true;
            this.lbl_NumCommande.Location = new System.Drawing.Point(30, 16);
            this.lbl_NumCommande.Name = "lbl_NumCommande";
            this.lbl_NumCommande.Size = new System.Drawing.Size(120, 13);
            this.lbl_NumCommande.TabIndex = 0;
            this.lbl_NumCommande.Text = "Numéro de commande :";
            // 
            // lbl_Price
            // 
            this.lbl_Price.AutoSize = true;
            this.lbl_Price.Location = new System.Drawing.Point(297, 16);
            this.lbl_Price.Name = "lbl_Price";
            this.lbl_Price.Size = new System.Drawing.Size(24, 13);
            this.lbl_Price.TabIndex = 1;
            this.lbl_Price.Text = "Prix";
            // 
            // lbl_NumCommande_Data
            // 
            this.lbl_NumCommande_Data.AutoSize = true;
            this.lbl_NumCommande_Data.Location = new System.Drawing.Point(156, 16);
            this.lbl_NumCommande_Data.Name = "lbl_NumCommande_Data";
            this.lbl_NumCommande_Data.Size = new System.Drawing.Size(13, 13);
            this.lbl_NumCommande_Data.TabIndex = 2;
            this.lbl_NumCommande_Data.Text = "0";
            // 
            // lbl_Price_Data
            // 
            this.lbl_Price_Data.AutoSize = true;
            this.lbl_Price_Data.Location = new System.Drawing.Point(340, 16);
            this.lbl_Price_Data.Name = "lbl_Price_Data";
            this.lbl_Price_Data.Size = new System.Drawing.Size(37, 13);
            this.lbl_Price_Data.TabIndex = 3;
            this.lbl_Price_Data.Text = "0 CHF";
            // 
            // cmb_Pasta
            // 
            this.cmb_Pasta.FormattingEnabled = true;
            this.cmb_Pasta.Items.AddRange(new object[] {
            "Classique",
            "Sans Gluten",
            "Pizza du chef"});
            this.cmb_Pasta.Location = new System.Drawing.Point(21, 42);
            this.cmb_Pasta.Name = "cmb_Pasta";
            this.cmb_Pasta.Size = new System.Drawing.Size(121, 21);
            this.cmb_Pasta.TabIndex = 4;
            this.cmb_Pasta.SelectedIndexChanged += new System.EventHandler(this.Cmb_Pasta_SelectedIndexChanged);
            // 
            // grp_Validation
            // 
            this.grp_Validation.Controls.Add(this.btn_Cancel);
            this.grp_Validation.Controls.Add(this.btn_Save);
            this.grp_Validation.Location = new System.Drawing.Point(52, 528);
            this.grp_Validation.Name = "grp_Validation";
            this.grp_Validation.Size = new System.Drawing.Size(581, 71);
            this.grp_Validation.TabIndex = 5;
            this.grp_Validation.TabStop = false;
            this.grp_Validation.Text = "Validation";
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(162, 39);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_Cancel.TabIndex = 0;
            this.btn_Cancel.Text = "Annuler";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.Btn_Cancel_Click);
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(270, 39);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(75, 23);
            this.btn_Save.TabIndex = 2;
            this.btn_Save.Text = "Enregistrer";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.Btn_Save_Click);
            // 
            // grp_data
            // 
            this.grp_data.Controls.Add(this.lbl_Price_Data);
            this.grp_data.Controls.Add(this.lbl_NumCommande_Data);
            this.grp_data.Controls.Add(this.lbl_NumCommande);
            this.grp_data.Controls.Add(this.lbl_Price);
            this.grp_data.Location = new System.Drawing.Point(87, 12);
            this.grp_data.Name = "grp_data";
            this.grp_data.Size = new System.Drawing.Size(393, 57);
            this.grp_data.TabIndex = 0;
            this.grp_data.TabStop = false;
            this.grp_data.Text = "Données de commande";
            this.grp_data.Enter += new System.EventHandler(this.GroupBox3_Enter);
            // 
            // grp_Pasta
            // 
            this.grp_Pasta.Controls.Add(this.cmb_Pasta);
            this.grp_Pasta.Location = new System.Drawing.Point(189, 88);
            this.grp_Pasta.Name = "grp_Pasta";
            this.grp_Pasta.Size = new System.Drawing.Size(200, 100);
            this.grp_Pasta.TabIndex = 4;
            this.grp_Pasta.TabStop = false;
            this.grp_Pasta.Text = "Type de pâte";
            // 
            // formConcept
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 651);
            this.Controls.Add(this.grp_Validation);
            this.Controls.Add(this.grp_Pasta);
            this.Controls.Add(this.grp_data);
            this.Controls.Add(this.groupBox1);
            this.Name = "formConcept";
            this.Text = "Concevoir sa pizza";
            this.Load += new System.EventHandler(this.Grp_Ingredients_Load);
            this.groupBox1.ResumeLayout(false);
            this.grp_Ingredients_unChosen.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.grp_Validation.ResumeLayout(false);
            this.grp_data.ResumeLayout(false);
            this.grp_data.PerformLayout();
            this.grp_Pasta.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox grp_Validation;
        private System.Windows.Forms.GroupBox grp_Pasta;
        private System.Windows.Forms.ComboBox cmb_Pasta;
        private System.Windows.Forms.Label lbl_Price_Data;
        private System.Windows.Forms.Label lbl_NumCommande_Data;
        private System.Windows.Forms.Label lbl_Price;
        private System.Windows.Forms.Label lbl_NumCommande;
        private System.Windows.Forms.GroupBox grp_data;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Button btn_Selector_Left;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_Selector_Right;
        private System.Windows.Forms.GroupBox grp_Ingredients_unChosen;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckedListBox chkList_Ingredients_Chosen;
        private System.Windows.Forms.CheckedListBox chkList_Ingredients_Avalible;
    }
}