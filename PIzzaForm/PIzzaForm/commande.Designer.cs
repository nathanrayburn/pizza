﻿namespace PIzzaForm
{
    partial class formAccueil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lst_commandes = new System.Windows.Forms.ListBox();
            this.btn_commande = new System.Windows.Forms.Button();
            this.lbl_Commande = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lst_commandes
            // 
            this.lst_commandes.FormattingEnabled = true;
            this.lst_commandes.Location = new System.Drawing.Point(82, 71);
            this.lst_commandes.Name = "lst_commandes";
            this.lst_commandes.Size = new System.Drawing.Size(116, 199);
            this.lst_commandes.TabIndex = 0;
            this.lst_commandes.SelectedIndexChanged += new System.EventHandler(this.Lst_commandes_SelectedIndexChanged);
            // 
            // btn_commande
            // 
            this.btn_commande.Location = new System.Drawing.Point(82, 293);
            this.btn_commande.Name = "btn_commande";
            this.btn_commande.Size = new System.Drawing.Size(116, 23);
            this.btn_commande.TabIndex = 1;
            this.btn_commande.Text = "Nouvelle Commande";
            this.btn_commande.UseVisualStyleBackColor = true;
            this.btn_commande.Click += new System.EventHandler(this.Btn_commande_Click);
            // 
            // lbl_Commande
            // 
            this.lbl_Commande.AutoSize = true;
            this.lbl_Commande.Location = new System.Drawing.Point(79, 38);
            this.lbl_Commande.Name = "lbl_Commande";
            this.lbl_Commande.Size = new System.Drawing.Size(86, 13);
            this.lbl_Commande.TabIndex = 2;
            this.lbl_Commande.Text = "Vos Commandes";
            this.lbl_Commande.Click += new System.EventHandler(this.Label1_Click);
            // 
            // formAccueil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lbl_Commande);
            this.Controls.Add(this.btn_commande);
            this.Controls.Add(this.lst_commandes);
            this.Name = "formAccueil";
            this.Text = "Page d\'accueil";
            this.Load += new System.EventHandler(this.FormAccueil_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lst_commandes;
        private System.Windows.Forms.Button btn_commande;
        private System.Windows.Forms.Label lbl_Commande;
    }
}