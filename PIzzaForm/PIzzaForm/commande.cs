﻿
using Classes.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;


namespace PIzzaForm
{
    public partial class formAccueil : Form
    {
        private List<Pizza> listPizza = null;
        private List<string> listPrices = null;

        public formAccueil()
        {
            InitializeComponent();
            float price = 0;
            int skip = 0;
            int skip2 = 0;
            int skipIngredient = 0;
            int id = 0;
            int pastaType = 0;

            List<string> listCommandes = null;
            List<string> listIngredients = null;


            List<Pizza> listPizza = new List<Pizza>();
            CSV content = new CSV();
            var list = content.ReadCommands;
            var listPrices = content.ReadIngredients;

     


            foreach (string pizza in list)
            {
                skip = 0;
                id = 0;
                pastaType = 0;
                skipIngredient = 0;


                List<Component> allIngredients = new List<Component>();
     
                listCommandes = pizza.Split(';').ToList(); //split de array of the csv content and turns into a list of string
 


           
                Int32.TryParse(listCommandes[0], out id); //Convert to int
                Int32.TryParse(listCommandes[1], out pastaType); // Convert to int




                foreach (string components in listCommandes)
                {
                    skip++;

                    if (skip <= 3 || components == "0"){ continue; } //Skipping the 3 first indexes

                    foreach (string prices in listPrices)
                    {

                        listIngredients = prices.Split(';').ToList(); //split de array of the csv content and turns into a list of string
                        
                        if (listIngredients[0] == components)
                        {
                            float.TryParse(listIngredients[1], out price);

                        }
                    }

                    Component ObjectIngredient = new Component(components, price); //Create an ingredient
                    if (listCommandes.Contains(components))
                    {
                        allIngredients.Add(ObjectIngredient);
                    }

                 
                }

                Pizza pizza1 = new Pizza(id, pastaType, allIngredients); //Create a new pizza
                listPizza.Add(pizza1);
            }

            listPizza.RemoveAt(0); //Remove the header from the Pizza list

            this.listPizza = listPizza; //Set the private list
            this.listPrices = listPrices; //set the private list
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void FormAccueil_Load(object sender, EventArgs e)
        {
     
            foreach (Pizza commande in listPizza)
            {

                lst_commandes.Items.Add(commande.ToString()); //Add the orders
            }

        }

        private void Lst_commandes_SelectedIndexChanged(object sender, EventArgs e)
        {
       

        
            
            string selected = lst_commandes.SelectedItem.ToString();
            Pizza send = null;
            bool closed = false;
     
            foreach (Pizza command in listPizza)
                {
                    var la = command.ToString();
                    if (la == selected)
                    {
                        send = command;

                    }


                }


            formConcept pizza = new formConcept(send, listPrices);
            pizza.ShowDialog(); //Open the pizza form



        }

        private void Btn_commande_Click(object sender, EventArgs e)
        {
            
            List<Component> ingredients = new List<Component>();
            Pizza send = new Pizza( 1, 0,ingredients);
            send.ID = send.newID;

            formConcept pizza = new formConcept(send, listPrices);

            pizza.ShowDialog(); //Open the pizza form
            this.DialogResult = DialogResult.OK;
            RestartProgram();
        }

        private void RestartProgram()
        {
            // Get file path of current process 
            var filePath = Assembly.GetExecutingAssembly().Location;
            //var filePath = Application.ExecutablePath;  // for WinForms

            // Start program
            Process.Start(filePath);

            // For Windows Forms app
            Application.Exit();

            // For all Windows application but typically for Console app.
            //Environment.Exit(0);
        }
    }
}
